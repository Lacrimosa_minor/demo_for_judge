from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.metrics import mean_squared_error
import joblib
import matplotlib.pyplot as plt
import numpy as np

from func import *

#模型训练
def train(X,Y,model):
    X_train,X_test,Y_train,Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True)
    models = {'linearR':LinearRegression(),
    'ridge':Ridge(alpha=0.5),
    'lasso':Lasso(alpha=0.1)}
    lm = models[model]
    saved_model = lm.fit(X_train,Y_train)
    Y_pred = lm.predict(X_test)
    
    joblib.dump(saved_model,'model.pkl')

    #输出训练指标
    print('training R^2:', lm.score(X_train, Y_train))
    print('testing  R^2:', lm.score(X_test, Y_test))
    rmse = (np.sqrt(mean_squared_error(Y_test, Y_pred)))
    print('rmse:', rmse)

    # 输出系数
    print('k:', lm.coef_)
    print('b:', lm.intercept_)

    # 输出测试集拟合效果图
    plt.scatter(Y_test, Y_pred)
    plt.xlabel("scores")
    plt.ylabel("Predicted prices")
    plt.title("scores vs Predicted scores")
    plt.show()

#单条demo测试
def demo_test(saved_model,test_text):
    if len(test_text)==0:
        print("InputError")
    else:
        test_feature = get_all_feature(test_text)
        filter_labels = ['hsk2','hsk3','hsk4','hsk5','hsk6'] #手工输入
        x_test = get_format_data(test_feature,filter_labels)
        model = joblib.load(saved_model)
        score = model.predict(x_test)
    return score


#------MODEL--TEST----------#
if __name__ =='__main__':
    
    #模型训练特征数据
    #file = 'score.xlsx' 
    #X,Y,filter_labels = get_train_data(file)
    
    #模型训练生成了model.pkl后无需再次训练
    #第三个参数是选择所需要的模型名字，在['linerR','ridge','lasso']中三选一
    #train(X,Y,'linearR')

    test_text = '弯弯的月儿小小的船。小小的船儿两头尖。我在小小的船里坐，只看见闪闪的星星蓝蓝的天'
    
    #传入字符串，生成特征分析结果的字典
    test_feature = get_all_feature(test_text)
    print(test_feature) 

    #传入字符串，生成预测分数
    predict_score = demo_test('model.pkl',test_text)
    print(predict_score)