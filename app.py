# -*- coding:utf-8 -*-
from flask import Flask, render_template, request, jsonify, send_from_directory
import logging
import urllib
import score_ml
import time
import os.path

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

@app.route('/')
def create_app(a, b):  # put application's code here
    print(a)
    print(b)
    # return Response("index.html", mimetype='text/html')
    return render_template("index.html", mimetype='text/html')

#
@app.route('/text_recieve/', methods=['POST'])
def text_analys():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
    log_path = os.path.dirname(os.getcwd()) + '/pylog/'
    log_name = log_path + rq + '.log'
    print(log_name)
    logfile = log_name
    fh = logging.FileHandler(logfile, mode='w')
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logging.debug("text recieved!")
    input_text = urllib.parse.unquote(str(request.data)[13: -1]).replace('\n', '').replace('\r', '').replace('    ', '')
    print("begin to analys")
    print(input_text)
    logging.debug(input_text)
    temp = score_ml.get_all_feature(input_text)
    outcome = dict()
    logging.debug("chart has been come out")
    value_name = ['tree_depth', 'sent_lenth',
                  'clause_lenth', 'multi_han',
                  'multi_word', 'hsk1', 'hsk2',
                  'hsk3', 'hsk4', 'hsk5', 'hsk6',
                  'chengyu', 'verb_num', 'adj_num', 'conj_num']
    idx = 0
    for item in temp.values():
        outcome[value_name[idx]] = item
        idx += 1
    outcome['complex_level'] = score_ml.demo_test('model.pkl', input_text)[0]
    print(outcome)
    return jsonify(outcome)

@app.route('/favicon.ico')#设置icon
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),#对于当前文件所在路径,比如这里是static下的favicon.ico
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run()
