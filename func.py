import xlrd
import re
import jieba
import jieba.posseg
import pandas as pd
import numpy as np
from zhon import hanzi
from scipy.stats import pearsonr
from ltp import LTP
import logging
from func_timeout import func_set_timeout
import func_timeout
#
# @func_set_timeout(30)
# logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',level =logging.DEBUG)


#提取出特征0：平均句法树深度
ltp = LTP("LTP/base2")


def get_avgDepth(file, pre_sent_split=False):
    if not pre_sent_split:
        text = list()
        text.append(file)
        text_list = ltp.sent_split(text)
    else:
        text_list = file

    seg, hidden = ltp.seg(text_list)
    dep_res = ltp.dep(hidden)
    # print(dep_res)
    depth_sum = 0
    # print(seg)
    for sent_dep in dep_res:
        sent_dep = [list(t) for t in sent_dep]
        tail2head = {t[0]: t[1] for t in sent_dep}

        for i in range(1, len(sent_dep) + 1):
            if tail2head[i] and tail2head[tail2head[i]] == i:  # 解除环
                sent_dep[tail2head[i] - 1][1] = 0

        node2depth = {i: 1 for i in range(1, len(sent_dep) + 1)}
        visited = {i: False for i in range(1, len(sent_dep) + 1)}
        visited[0] = True  # 0是根节点
        tree_depth = 0
        curNode = 1
        i = 0

        while curNode <= len(sent_dep):
            if visited[curNode]:
                curNode += 1
                continue
            visited[curNode] = True

            curPa = sent_dep[curNode - 1][1]
            i = 0
            while curPa != 0:  # 更新当前节点的深度
                i += 1
                # print(curNode, curPa)
                if curPa == curNode:  # 禁止环
                    break

                if visited[curPa]:
                    node2depth[curNode] += node2depth[curPa]
                    break

                node2depth[curNode] += 1
                curPa = sent_dep[curPa - 1][1]
                if i > 10:  # 防止死循环，禁止环后一般不会触发
                    break
            tree_depth = max(tree_depth, node2depth[curNode])
            curPa = sent_dep[curNode - 1][1]
            dist = 1
            while not visited[curPa]:  # 更新当前节点的父节点的深度
                node2depth[curPa] = node2depth[curNode] - dist
                dist += 1
                visited[curPa] = True
            curNode += 1

        depth_sum += tree_depth
    return depth_sum / len(dep_res)

#提取出特征1：平均大句长(含标点符号)
def get_avgLenth(file):
    juzi = re.findall(hanzi.sentence,file)
    s1 = 0
    for b in juzi:
        s1 += len(b)
    dajuchang = s1 / len(juzi)
    return dajuchang

#提取出特征2：平均小句长（不含标点符号）
def get_avgClauseLenth(file):
    juzi = re.findall(hanzi.sentence,file)
    l2 = []
    for j in juzi:
        xiaoju = j.split('，')
        l2.extend(xiaoju)
    s = 0
    for x in l2:
        s += len(x)
    xiaojuchang = (s-len(juzi)) / len(l2)
    return xiaojuchang

#提取特征3：汉字多样性,即不重复的汉字数 / 所有汉字数
def get_multiHan(file):
    l3 = []
    for x in file:
        if x.isalpha():
            l3.append(x)
    buchongfuhanzishu = len(set(l3))
    suoyouhanzishu = len(l3)
    result = buchongfuhanzishu / suoyouhanzishu
    return result

#提取特征4：词汇多样性（即不重复的词语数/所有词语数）
def get_multiWord(file):
    wordlist = list(jieba.cut(file))
    wordlist = [x for x in wordlist if x.isalpha()]
    #print(wordlist)
    suoyouciyushu = len(wordlist)
    buchongfuciyushu = len(set(wordlist))
    result = buchongfuciyushu / suoyouciyushu
    return result

#提取特征5：各级词汇占比
#提取各级词表
def get_Word_Distribution(file):
    data = xlrd.open_workbook('HSK-2012.xls')
    s = data.sheets()[0]
    s1 = s.col_values(0)[0:150]
    l1 = []
    for x in s1:
        r1 = re.match('\w', x)
        l1.append(r1.group())
    s2 = s.col_values(0)[150:300]
    l2 = []
    for x in s2:
        r2 = re.match('\w', x)
        l2.append(r2.group())
    s3 = s.col_values(0)[300:600]
    l3 = []
    for x in s3:
        r3 = re.match('\w', x)
        l3.append(r3.group())
    s4 = s.col_values(0)[600:1200]
    l4 = []
    for x in s4:
        r4 = re.match('\w', x)
        l4.append(r4.group())
    s5 = s.col_values(0)[1200:2500]
    l5 = []
    for x in s5:
        r5 = re.match('\w', x)
        l5.append(r5.group())
    s6 = s.col_values(0)[2500:5000]
    l6 = []
    for x in s6:
        r6 = re.match('\w', x)
        l6.append(r6.group())

    wordlist = list(jieba.cut(file))
    c1,c2,c3,c4,c5,c6 =0,0,0,0,0,0
    for x in wordlist:
        if x in l1:
            c1 += 1
        elif x in l2:
            c2 += 1
        elif x in l3:
            c3 += 1
        elif x in l4:
            c4 += 1
        elif x in l5:
            c5 += 1
        elif x in l6:
            c6 += 1
    result = [c1/len(wordlist),c2/len(wordlist),c3/len(wordlist),c4/len(wordlist),c5/len(wordlist),c6/len(wordlist)]
    return result


#可能特征6：使用四字词语或成语数量
def get_4NumWord(file):
    wordlist2 = list(jieba.cut(file))
    wordlist2 = [x for x in wordlist2 if x.isalpha() and len(x) == 4]
    return len(wordlist2)

#可能特征7：使用不重复动词数量（鉴于动词可以使作文灵动）
def get_VerbNum(file):
    d = []
    wordlist3 = jieba.posseg.cut(file)
    for k,v in wordlist3:
        if v == 'v':
            d.append(k)
    d = set(d)
    return len(d)


#可能特征8：使用不重复形容词数量（鉴于形容词可以使作文绘声绘色，丰富多彩）
def get_AdjNum(file):
    d = []
    wordlist4 = jieba.posseg.cut(file)
    for k,v in wordlist4:
        if v == 'a':
            d.append(k)
    d = set(d)
    return len(d)

#可能特征9：使用不重复连词数量（鉴于连词可以使作文前后衔接连贯）
def get_ConjNum(file):
    d = []
    wordlist5 = jieba.posseg.cut(file)
    for k,v in wordlist5:
        if v == 'c':
            d.append(k)
    d = set(d)
    return len(d)

#一键得到所有特征字典
def get_all_feature(file):
    try:
        avgDepth = get_avgDepth(file)
    except func_set_timeout.exceptions.FunctionTimedOut:
        logging.debug(f'函数执行超时，输入文本内容为：{file}')
    avgLenth = get_avgLenth(file)
    avgClauseLenth = get_avgClauseLenth(file)
    multiHan = get_multiHan(file)
    multiWord = get_multiWord(file)
    word_distribution_list = get_Word_Distribution(file)
    w1 = word_distribution_list[0]
    w2 = word_distribution_list[1]
    w3 = word_distribution_list[2]
    w4 = word_distribution_list[3]
    w5 = word_distribution_list[4]
    w6 = word_distribution_list[5]
        
    num4Word = get_4NumWord(file)
    verbNum =get_VerbNum(file)
    adjNum = get_AdjNum(file)
    conjNum = get_ConjNum(file)

    result = {
    '平均句法树深度':avgDepth,
    '平均句长':avgLenth,
    '平均小句长':avgClauseLenth,
    '汉字多样性':multiHan,
    '词汇多样性':multiWord,
    '一级词占比':w1,
    '二级词占比':w2,
    '三级词占比':w3,
    '四级词占比':w4,
    '五级词占比':w5,
    '六级词占比':w6,
    '四字成语数':num4Word,
    '不重复动词数':verbNum,
    '不重复形容词数':adjNum,
    '不重复连词数':conjNum
    }
    return result


#训练数据转换和预处理
def get_train_data(file):
    score_df = pd.read_excel(file)
    scores = score_df['level']
    Y = np.array(scores)
    X = []
    labels = ['tree_depth','sent_lenth','clause_lenth','multi_han','multi_word','hsk1','hsk2','hsk3','hsk4','hsk5','hsk6','chengyu','verb_num','adj_num','conj_num']
    filter_labels = []
    for label in labels:
        nums = score_df[label]
        r, p = pearsonr(scores,nums)
        #print(label,r,p)
        if p <0.05 and abs(r)>0.1:
            X.append(np.array(nums))
        else:
            filter_labels.append(label)
            #print(label,'is filtered')

    X = np.array(X)
    X = X.T

    return X,Y,filter_labels


#单独传入一条测试文本的特征字典生成模型预测所需矩阵格式
def get_format_data(feature,filter_labels): 
    #filter_labels,由训练数据特征相关分析结果得到过滤掉的特征label
    
    text_dict_en = {
        'tree_depth':[],
        'sent_lenth':[],
        'clause_lenth':[],
        'multi_han':[],
        'multi_word':[],
        'hsk1':[],
        'hsk2':[],
        'hsk3':[],
        'hsk4':[],
        'hsk5':[],
        'hsk6':[],
        'chengyu':[],
        'verb_num':[],
        'adj_num':[],
        'conj_num':[]
        }
    
    text_dict_en['tree_depth'].append(feature['平均句法树深度'])
    text_dict_en['sent_lenth'].append(feature['平均句长'])
    text_dict_en['clause_lenth'].append(feature['平均小句长'])
    text_dict_en['multi_han'].append(feature['汉字多样性'])
    text_dict_en['multi_word'].append(feature['词汇多样性'])
    text_dict_en['hsk1'].append(feature['一级词占比'])
    text_dict_en['hsk2'].append(feature['二级词占比'])
    text_dict_en['hsk3'].append(feature['三级词占比'])
    text_dict_en['hsk4'].append(feature['四级词占比'])
    text_dict_en['hsk5'].append(feature['五级词占比'])
    text_dict_en['hsk6'].append(feature['六级词占比'])
    text_dict_en['chengyu'].append(feature['四字成语数'])
    text_dict_en['verb_num'].append(feature['不重复动词数'])
    text_dict_en['adj_num'].append(feature['不重复形容词数'])
    text_dict_en['conj_num'].append(feature['不重复连词数'])

    test_df = pd.DataFrame(text_dict_en)
    labels = ['tree_depth','sent_lenth','clause_lenth','multi_han','multi_word','hsk1','hsk2','hsk3','hsk4','hsk5','hsk6','chengyu','verb_num','adj_num','conj_num']
    x_test  = []
    for label in labels:
        if label not in filter_labels:
            num = test_df[label]
            x_test.append(np.array(num))
    
    x_test = np.array(x_test)
    x_test = x_test.T

    return x_test



#------processing-----#

if __name__ == '__main__':

    #批量处理模型训练使用的语料
    df = pd.read_excel('文本.xlsx')

    text_dict_en = {
        'content':[],
        'tree_depth':[],
        'sent_lenth':[],
        'clause_lenth':[],
        'multi_han':[],
        'multi_word':[],
        'hsk1':[],
        'hsk2':[],
        'hsk3':[],
        'hsk4':[],
        'hsk5':[],
        'hsk6':[],
        'chengyu':[],
        'verb_num':[],
        'adj_num':[],
        'conj_num':[]
        }
    n = 0
    for text in df['文本内容']:
        feature = get_all_feature(text)
        text_dict_en['content'].append(text)
        text_dict_en['tree_depth'].append(feature['平均句法树深度'])
        text_dict_en['sent_lenth'].append(feature['平均句长'])
        text_dict_en['clause_lenth'].append(feature['平均小句长'])
        text_dict_en['multi_han'].append(feature['汉字多样性'])
        text_dict_en['multi_word'].append(feature['词汇多样性'])
        text_dict_en['hsk1'].append(feature['一级词占比'])
        text_dict_en['hsk2'].append(feature['二级词占比'])
        text_dict_en['hsk3'].append(feature['三级词占比'])
        text_dict_en['hsk4'].append(feature['四级词占比'])
        text_dict_en['hsk5'].append(feature['五级词占比'])
        text_dict_en['hsk6'].append(feature['六级词占比'])
        text_dict_en['chengyu'].append(feature['四字成语数'])
        text_dict_en['verb_num'].append(feature['不重复动词数'])
        text_dict_en['adj_num'].append(feature['不重复形容词数'])
        text_dict_en['conj_num'].append(feature['不重复连词数'])
        n += 1
        if n % 50 ==0:
            print('当前进度：'+str(n)+'/443')    

    grade_map = {'一年级':1,'二年级':2,'三年级':3,'四年级':4,'五年级':5,'六年级':6}

    text_df = pd.DataFrame(text_dict_en)
    text_df['level'] = df['年级数'].map(grade_map)
    text_df['content'] = df['文本内容']
    text_df.to_excel('score.xlsx',index=False)

